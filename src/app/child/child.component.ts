import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AbstractControl, UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'app-child',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent {
  @Output() clearFilterResult = new EventEmitter<boolean>();

  public clearFilter(filter: AbstractControl | UntypedFormControl, name: string) {
    filter.reset(this.pristineForm[name]);
    filter.markAsPristine();
    this.clearFilterResult.emit(true);// gdy button jest wciśniety, to tutaj przekazujemy true do rodzica.
  }

}
