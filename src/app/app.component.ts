import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppSerService } from './app-ser.service';
import { AbstractControl, UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  constructor(private appSerService: AppSerService) 
  { 
    
  }

  clearFilterResult(data: boolean) {
    this.formGroup.get('days').value.forEach((day: any, i) => {
      if (day.from === null || day.to === null) {
        this.days[i].time = "";
      }
    });
    this.countTime();
  }

  ngOnInit() {
  }
}
